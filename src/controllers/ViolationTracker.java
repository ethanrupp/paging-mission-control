package controllers;

import java.time.LocalDateTime;

import models.DataEntry;

public class ViolationTracker {
	
	private int tstatViolations;
	private int battViolations;
	DataEntry oldestTstatData;
	DataEntry oldestBattData;
	
	
	public ViolationTracker() {
		tstatViolations = 0;
		battViolations = 0;
		oldestTstatData = null;
		oldestBattData = null;
	}
	
	public DataEntry getOldestTstatData() {
		return oldestTstatData;
	}

	public void setOldestTstatData(DataEntry oldestTstatData) {
		this.oldestTstatData = oldestTstatData;
	}

	public DataEntry getOldestBattData() {
		return oldestBattData;
	}

	public void setOldestBattData(DataEntry oldestBattData) {
		this.oldestBattData = oldestBattData;
	}

	public int getTstatViolations() {
		return tstatViolations;
	}

	public void setTstatViolations(int tstatViolations) {
		this.tstatViolations = tstatViolations;
	}

	public int getBattViolations() {
		return battViolations;
	}

	public void setBattViolations(int battViolations) {
		this.battViolations = battViolations;
	}

	public void setoldestTstatData(DataEntry timestamp) {
		oldestTstatData = timestamp;
	}
	
	public void addtstatViolation() {
		this.tstatViolations++;
	}
	public void addbattViolation() {
		this.battViolations++;
	}
}
