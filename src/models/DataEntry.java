package models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DataEntry {
	
	
	private int id;
	private int redHigh;
	private int redLow;
	private int yellowHigh;
	private int yellowLow;
	private double rawValue;
	private String component;
	private LocalDateTime timestamp;


	public DataEntry(String[] split) throws Exception {
		parseTimestamp(split[0]);
		this.id = Integer.parseInt(split[1]);
		this.redHigh = Integer.parseInt(split[2]);
		this.yellowHigh = Integer.parseInt(split[3]);
		this.redLow = Integer.parseInt(split[4]);
		this.yellowLow = Integer.parseInt(split[5]);
		this.rawValue = Double.parseDouble(split[6]);
		this.component = split[7];
	}
	
	public void parseTimestamp(String timeString) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        timestamp = LocalDateTime.parse(timeString, formatter);
	}

	//GETTERS AND SETTERS
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRedHigh() {
		return redHigh;
	}
	public void setRedHigh(int redHigh) {
		this.redHigh = redHigh;
	}
	public int getRedLow() {
		return redLow;
	}
	public void setRedLow(int redLow) {
		this.redLow = redLow;
	}
	public int getYellowHigh() {
		return yellowHigh;
	}
	public void setYellowHigh(int yellowHigh) {
		this.yellowHigh = yellowHigh;
	}
	public int getYellowLow() {
		return yellowLow;
	}
	public void setYellowLow(int yellowLow) {
		this.yellowLow = yellowLow;
	}
	public double getRawValue() {
		return rawValue;
	}
	public void setRawValue(double rawValue) {
		this.rawValue = rawValue;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
}