package pagingmissioncontrol;

import java.io.BufferedReader;
import java.util.ArrayList; 
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.logging.Logger;

import controllers.ViolationTracker;
import models.DataEntry;
import constants.Path;


public class App {

	private static final Logger log = Logger.getGlobal();
	private static ArrayList<String> list = new ArrayList<String>();
	
	public static void main(String[] args) {
		//log.info("Initializing...");
		BufferedReader reader = null;
		HashMap<Integer, ViolationTracker> trackersById = new HashMap<>();
		try {
			reader = new BufferedReader((new FileReader(Path.SAMPLEDATA)));
			
			String line = null;
			while((line = reader.readLine()) != null) {
				
				DataEntry data = processLine(line);
				
				if(data.getComponent().equals("TSTAT") && data.getRawValue()>data.getRedHigh()) {
					if(trackersById.get(data.getId())==null) {
						trackersById.put(data.getId(), new ViolationTracker());
					}
					ViolationTracker tracker = trackersById.get(data.getId());
					
					if(tracker.getTstatViolations()==0) {
						tracker.setOldestTstatData(data);
					}
					
					tracker.addtstatViolation();
					
					if(tracker.getTstatViolations()>=3) {
						if(tracker.getOldestTstatData().getTimestamp().plusMinutes(5).isAfter(data.getTimestamp())) {
							list.add(makeAlertMessage(tracker.getOldestTstatData())); // change to post oldest timestamp
							tracker.setTstatViolations(0);
						} else {
							tracker.setTstatViolations(0);
						}
					}
				}
				else if(data.getComponent().equals("BATT") && data.getRawValue()<data.getRedLow()) {
					if(trackersById.get(data.getId())==null) {
						trackersById.put(data.getId(), new ViolationTracker());
					}
					ViolationTracker tracker = trackersById.get(data.getId());
					
					if(tracker.getBattViolations()==0) {
						tracker.setOldestBattData(data);
					}
					tracker.addbattViolation();
					
					if(tracker.getBattViolations()>=3) {
						if(tracker.getOldestTstatData().getTimestamp().plusMinutes(5).isAfter(data.getTimestamp())) {
							list.add(makeAlertMessage(tracker.getOldestBattData())); // change to post oldest timestamp
							tracker.setBattViolations(0);
						} else {
							tracker.setBattViolations(0);
						}
					}
				}
				else {
					log.fine("400 Bad Request: No component found");
				}
			}
		} catch(Exception e) {
			log.severe("Fatal error occurred during program runtime.");
			e.printStackTrace();
		}
		
		for(int i = 0; i < list.size()-1; i ++) {
			System.out.print(list.get(i)+",\n");
		}
		System.out.println(list.get(list.size()-1));
		
		try {
			reader.close();
		} catch (IOException e) {
			log.severe("BufferedReader could not be closed.");
			e.printStackTrace();
		}
		//log.info("Program execution finished.");
	}
	
	public static DataEntry processLine(String line) throws Exception {
		String[] split = line.split("\\|");
		if(split.length!=8) {
			throw new Exception("400 Bad Request: Data not properly formatted");
		}
		return new DataEntry(split);
	}
	
	private static String makeAlertMessage(DataEntry data) {
		//note: I am not using any dependencies or even maven to build this project
		//therefore, I am manually constructing the output.
		//I have experience with the  JSONObject class from the apache library, but I lack that here
		String severity;
		if(data.getComponent().equals("BATT")) {
			severity = "RED LOW";
		}
		else {
			severity = "RED HIGH";
		}
		String output = 
				"{\r\n" +
		        "	\"satelliteId\": " +data.getId()+",\r\n" +
		        "	\"severity\": \""+ severity + "\",\r\n" +
		        "	\"component\": \""+ data.getComponent()+"\",\r\n" +
		        "	\"timestamp\": \""+ data.getTimestamp().toString() + "Z\"" +
		        "\r\n}";
		return output;
	}
}
